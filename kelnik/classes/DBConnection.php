<?php

class DBConnection {
        protected static $_instance;  
  
        public static function getInstance() { 
            if (self::$_instance === null) { 
                self::$_instance = new self;  
            } 
            return self::$_instance; 
        }
   
        private  function __construct() { 
                //подключение к БД  
            //...
        }
 
        private function __clone() { 
        }
        
        private function __wakeup() {
        }
        
        //...         
}