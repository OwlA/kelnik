<?php
class XmlImporter extends FileImporter {
	function process() {
		try {
			$xmlObj = simplexml_load_file ( $this->filename );
			$arr = [];
			$i=0;
			foreach ( $xmlObj as $el ) {
				$arrNodes = (array) $el;
				if ( is_array( $arrNodes[ "@attributes" ] )) {
					$arr[$i] = array_merge( $arrNodes[ "@attributes" ], $arrNodes );
					unset( $arr[$i][ "@attributes" ] );
				}
				else {
					$arr[$i] = $arrNodes;
				}
				$i++;
			}
		}
		catch ( Exception $e ) {
			echo $e->_toString();
			return false;
		}
		return $arr;
	}
}