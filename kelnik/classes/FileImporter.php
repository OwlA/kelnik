<?php
abstract class FileImporter {
	protected $filename;

	function __construct( $filename ){
		$this->filename = $filename;
	}

	public function import( DBConnection $connection, $table ) {
            $arr = $this -> process();
            //запись в БД
            //...
        }  

	static function getInstance( $filename ){
		if ( preg_match( "/\.xml$/i", $filename )) { 
            return new XmlImporter( $filename );
        }
        elseif ( preg_match( "/\.csv$/i", $filename )) {
            return new CsvImporter( $filename );
        }
        else {
            echo 'Импорт из файла данного формата не предусмотрен';
        }
	}

	abstract function process();
}