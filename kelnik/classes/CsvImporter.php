<?php
class CsvImporter extends FileImporter {
	function process() {
		$row = 0;
		try {
			if(($handle = fopen ( $this->filename , 'r'))  !== false) {
				$arr = [];
				$num = 0;
				$fields = [];				
				while (($data = fgetcsv( $handle, 0, ';')) !== false ) {
					if ( $row == 0 ){
						$num = count( $data );
						$fields = $data;
					}
					else {
						 for ( $i = 0; $i < 4; $i++ ) {
							$arr [$row - 1][$fields [$i]] = $data [$i];
						}
					}
					$row++ ;
				}
			}
			
		}
		catch ( Exception $e ) {
			echo $e->_toString();
			return false;
		}
		finally {
			fclose ( $handle );
		}
		return $arr;
	}
}