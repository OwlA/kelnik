<?php

require_once ( 'classes/FileImporter.php' );
require_once ( 'classes/XmlImporter.php' );
require_once ( 'classes/CsvImporter.php' );


$arr = FileImporter::getInstance( 'data.csv' ) -> process();
print_r( $arr );

echo "<br>";

$arr = FileImporter::getInstance( 'data.xml' ) -> process();
print_r( $arr );